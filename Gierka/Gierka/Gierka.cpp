#include "Gierka.h"
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <sstream>
#include <cstdlib>
#include <functional>
using namespace std;

bool collision(Player &player, Coin& coin, bool sound, int &counter) // Checking the collision 
{
	if (player.x + coin.width<coin.xc || player.x>coin.xc + coin.width || player.y + coin.height <coin.yc || player.y>coin.yc + coin.height)
	{
		//NO COLLISION
		return false;
	}
	else
	{
		if (player.level >= 0)
		{
			coin.xc = rand() % (ScreenWidth - 23);
			coin.yc = rand() % 210 + 30;
		}
		if (player.level >= 1)
		{
			coin.horizontalMovement = rand() % 2;
			coin.verticalMovement = rand() % 2;
		}
		if (sound)
			al_play_sample(coin.soundEffect, 1, 0, 3, ALLEGRO_PLAYMODE_ONCE, 0);
		if (coin.timeCoin)
		{
			counter++;
			player.sec += 10;
		}
		if(coin.slowdownCoin)
		{
			counter++;
		}
		else
			player.score++;
		
		return true;
	}
}
void saveGame(Player player) // saveing game state to binary file
{
	ofstream plikout("gamedata.bin", ios::binary);  //| ios::app
	plikout.write((char*)&player, sizeof(player));
	plikout.close();
}
void loadGame(Player & player)
{
	ifstream plikin("gamedata.bin", ios::binary);
	if (plikin.good() == false)
		cout << "Brak stanu gry do wczytania!" << endl;
	if (player.sec > 1)
	{
		plikin.read((char*)&player, sizeof(player));
	}
}
void WinnerLoad(vector<Winner> & winner)	// loading score from binary file
{
	ifstream plikin("winner.bin", ios::binary);
	if (plikin.good() == false)
		cout << "Brak stanu gry do wczytania!" << endl;
	plikin.read((char*)&winner[0], winner.size() * sizeof(Winner));
	sort(winner.begin(), winner.end(), greater<Winner>());
}
void WinnerSave(vector<Winner> & winner, Player player) // saving score to binary file
{
	sort(winner.begin(), winner.end(), greater<Winner>());
	for (int i = 0; i < 3; i++)
	{
		if (winner[i].score < player.score || winner[i].score == 0)
		{

			winner[i].score = player.score;
			winner[i].level = player.level;
			break;

			//strcpy(winner.name, player.name);
		}
	}

	sort(winner.begin(), winner.end(), greater<Winner>());



	ofstream plikout("winner.bin", ios::binary);
	plikout.write((char*)&winner[0], winner.size() * sizeof(Winner));
	plikout.close();
}
void WinnerShow(vector <Winner> winner)		//console representation of new highscore
{
	for (int i = 0; i < 3; i++)
	{
		cout << winner[i].score << endl;
		cout << winner[i].level << endl;
		//cout << winner.name << endl;
		cout << "\n";
	}
}
