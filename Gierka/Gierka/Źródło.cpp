#include <iostream>
#include <allegro5/allegro.h>
#include <allegro5/allegro_primitives.h>
#include <allegro5/allegro_native_dialog.h>
#include <allegro5\allegro_image.h>
#include <allegro5\allegro_audio.h>
#include <allegro5\allegro_acodec.h>
#include <allegro5\allegro_font.h>
#include <allegro5\allegro_ttf.h>
#include <sstream>
#include <string>
#include <algorithm>
#include <cstdlib>
#include <fstream>
#include <vector>
#include <functional>
#include "Gierka.h"




using namespace std;

#define ScreenWidth 800		// defining screen resolution
#define ScreenHeight 600

int main()
{
	srand(time(NULL));
	bool done = false, draw = true, active = false, jump = false, sound = true, timecoin = true, slowdown = false;
	bool pause = true;
	
	enum Direction { DOWN, LEFT, RIGHT, UP };



	const float FPS = 60;
	const float framePS = 30;
	float chmurkiX = 0;
	int sec = 45, counter = 1, slowDownTimer = 0;
	int groundHeight = 420;
	int dir = DOWN;
	float mx = ScreenWidth / 2, my = ScreenHeight / 2;

	float movespeed = 5, jumpspeed = 20;
	const float gravity = 0.55;
	int degrees = sec * 8;
	float volume = 0.5;

	if (!al_init())
	{
		cout << " error" << endl;
		return -1;
	}
	else cout << "Allegro initialized properly" << endl;

	al_install_keyboard();
	al_install_mouse();
	al_init_image_addon();
	al_init_acodec_addon();
	al_install_audio();
	al_init_primitives_addon();
	al_init_font_addon();
	al_init_ttf_addon();


	al_set_new_display_flags(ALLEGRO_WINDOWED | ALLEGRO_RESIZABLE);
	ALLEGRO_DISPLAY* display = al_create_display(ScreenWidth, ScreenHeight);
	al_set_window_position(display, 500, 250);
	al_set_window_title(display, "Gierka");

	if (!display)
	{
		al_show_native_message_box(display, "Error", "Display error", "Something went wrong!", NULL, ALLEGRO_MESSAGEBOX_ERROR);
	}
	ALLEGRO_MOUSE_STATE mouseState;
	ALLEGRO_KEYBOARD_STATE keyState;
	al_reserve_samples(3);

	ALLEGRO_COLOR MenuGray = al_map_rgb(192, 205, 226);
	ALLEGRO_COLOR DarkBlue = al_map_rgb(35, 58, 81);
	ALLEGRO_BITMAP * player = al_load_bitmap("assets\\bitmaps\\boy.png"); // main player
	ALLEGRO_BITMAP * player2 = al_load_bitmap("assets\\bitmaps\\player1.png");	// highscore panel textures	
	ALLEGRO_BITMAP * player1 = al_load_bitmap("assets\\bitmaps\\girl.png");		// highscore panel textures	
	ALLEGRO_BITMAP * ground = al_load_bitmap("assets\\bitmaps\\tlo.png");
	ALLEGRO_BITMAP * chmurki = al_load_bitmap("assets\\bitmaps\\chmurki.png");
	ALLEGRO_BITMAP * gierka = al_load_bitmap("assets\\bitmaps\\gierka.png");
	ALLEGRO_BITMAP * zegar = al_load_bitmap("assets\\bitmaps\\zegar2.png");
	ALLEGRO_BITMAP * wskazowka = al_load_bitmap("assets\\bitmaps\\wskazowka2.png");
	ALLEGRO_BITMAP * instructionPanel = al_load_bitmap("assets\\bitmaps\\instruction.png");
	ALLEGRO_FONT *font = al_load_font("assets\\fonts\\Minecraft.ttf", 25, ALLEGRO_ALIGN_CENTER);
	ALLEGRO_FONT *font2 = al_load_font("assets\\fonts\\Minecraft.ttf", 40, ALLEGRO_ALIGN_CENTER);


	ALLEGRO_SAMPLE * song = al_load_sample("assets\\sounds\\music.wav");

	ALLEGRO_TIMER *timer = al_create_timer(1.0 / FPS);
	ALLEGRO_TIMER *FrameTimer = al_create_timer(1.0 / framePS);
	ALLEGRO_TIMER *secTimer = al_create_timer(1);

	ALLEGRO_EVENT_QUEUE *eventqueue = al_create_event_queue();
	ALLEGRO_EVENT events;

	al_register_event_source(eventqueue, al_get_mouse_event_source());
	al_register_event_source(eventqueue, al_get_keyboard_event_source());
	al_register_event_source(eventqueue, al_get_timer_event_source(timer));
	al_register_event_source(eventqueue, al_get_timer_event_source(FrameTimer));
	al_register_event_source(eventqueue, al_get_timer_event_source(secTimer));
	al_register_event_source(eventqueue, al_get_display_event_source(display));



	Player p1;
	Player p;
	vector<Winner> winner(3);
	WinnerLoad(winner);
	loadGame(p);
	for (int i = 0; i < 1; i++) // Console test
	{
		cout << "Level: " << p.level << endl;
		cout << "X: " << p.x << endl;
		cout << "Y: " << p.y << endl;
		cout << "Score: " << p.score << endl;
		cout << "Sec: " << p.sec << endl;
	}
	
	Coin C[5];
	C[1] = { true,true,0,0,4 };
	C[2] = { true,false,700,450,6 };
	C[3] = { false,true,450,300,5 };
	C[4] = { false,false,0,400,3 };

	Coin timerCoin = { true,true,0,0,5, al_load_bitmap("assets\\bitmaps\\timecoin.png"),true };
	
	Coin slowdownCoin = { true,true,200,300,5, al_load_bitmap("assets\\bitmaps\\slowdowncoin.png"),false,true };

	Menu m[5];	// Menu elements
	m[0] = { 250,ScreenHeight / 2,300,50 };
	m[1] = { 250,ScreenHeight / 2 + 75,300,50 };
	m[2] = { 250,ScreenHeight / 2 + 2 * 75,300,50 };
	m[4] = { 250,ScreenHeight / 2 + 3 * 75,300,50 };

	al_start_timer(timer);
	al_start_timer(FrameTimer);
	al_start_timer(secTimer);
	
	Menu soundon = { 10,ScreenHeight - 50,36,36,al_load_bitmap("assets\\bitmaps\\soundon.png"), al_load_bitmap("assets\\bitmaps\\soundonFILLED.png") };
	Menu soundoff = { 50,ScreenHeight - 50,36,36,al_load_bitmap("assets\\bitmaps\\soundoff.png"), al_load_bitmap("assets\\bitmaps\\soundoffFILLED.png") };
	Menu instruction = { 10,ScreenHeight - 100 ,36,36,al_load_bitmap("assets\\bitmaps\\question.png"), al_load_bitmap("assets\\bitmaps\\questionFILLED.png") };
	

	ALLEGRO_SAMPLE_INSTANCE *songInstance = al_create_sample_instance(song);
	al_set_sample_instance_playmode(songInstance, ALLEGRO_PLAYMODE_LOOP);
	al_attach_sample_instance_to_mixer(songInstance, al_get_default_mixer());
	al_play_sample_instance(songInstance);
	al_set_sample_instance_gain(songInstance, volume);

		
	done = false;

	while (!done)
	{
		draw = true;
		al_get_mouse_state(&mouseState);
		al_wait_for_event(eventqueue, &events);
		if (events.type == ALLEGRO_EVENT_MOUSE_AXES)
		{
			mx = events.mouse.x;
			my = events.mouse.y;
		}
		else if (events.type == ALLEGRO_EVENT_MOUSE_BUTTON_DOWN)
		{
			if (events.mouse.button & 1)
			{
				if (m[0].mousedetection(mx, my))
				{
					done = true;
					p1 = { 400,10,0,0,45,32,0,0,0 };
					movespeed = 5;
				}
				else if (m[1].mousedetection(mx, my))
				{
					if (!(p.sec < 1 || p.sec == 45))
					{
						loadGame(p1);
						done = true;
					}
				}
				else if (m[2].mousedetection(mx, my))
				{

					WinnerShow(winner);
					for (int i = 0; i < 3; i++) {
						al_draw_textf(font2, MenuGray, ScreenWidth / 2 - 300, ScreenHeight / 2 + 50 * i, NULL, "miejsce %i: %i", i + 1, winner[i]);
						al_draw_bitmap_region(player, 0, 0, 32, 48, ScreenWidth / 2, 275, NULL);
						al_draw_bitmap_region(player1, 0, 0, 32, 48, ScreenWidth / 2, 325, NULL);
						al_draw_bitmap_region(player2, 0, 0, 32, 48, ScreenWidth / 2, 375, NULL);
						al_flip_display();
					}
					al_rest(5);
					
				}
				
				else if (m[4].mousedetection(mx, my))
					exit(0);
				
				else if (soundon.mousedetection(mx, my))
					if (sound)
					{
						sound = false;
						volume = 0;
					}
					else
					{
						sound = true;
						volume = 0.5;
					}
			}
		}
		else if (events.type == ALLEGRO_EVENT_DISPLAY_CLOSE)
			exit(0);
		
		al_set_sample_instance_gain(songInstance, volume);
		

		if (draw)
		{
			draw = false;
			for (int i=1; i <=3 ; i++)
			{
				C[i].CoinMovement(2, true);
				C[i].DrawCoin();
			}
			C[4].CoinMovement(1, true);
			C[4].DrawCoin();
			
			
			al_draw_bitmap(gierka, 125 , ScreenHeight / 5, NULL);
			m[0].drawMenu("\t\tNEW GAME", MenuGray, DarkBlue,MenuGray, m[0].mousedetection(mx, my));
			m[1].drawMenu("\t\tLOAD GAME", MenuGray, DarkBlue, MenuGray, m[1].mousedetection(mx, my));
			m[2].drawMenu("HIGH SCORES", MenuGray, DarkBlue, MenuGray, m[2].mousedetection(mx, my));
			//m[3].drawMenu("\t\tHOW TO PLAY", MenuGray, DarkBlue, MenuGray, m[3].mousedetection(mx, my));
			m[4].drawMenu("\t\tEXIT GAME", MenuGray, DarkBlue, MenuGray, m[4].mousedetection(mx, my));
			if ((p.sec < 1 || p.sec == 45)& m[1].mousedetection(mx, my))
			{
				al_draw_filled_rectangle(250, ScreenHeight / 2 + 75, 500, ScreenHeight/2+120,MenuGray);
				al_draw_text(m[1].font, DarkBlue, 295, ScreenHeight / 2 + 82, NULL, "NO GAME TO LOAD");
			}
			instruction.drawIcon(instruction.mousedetection(mx, my));
			if(sound)
			soundon.drawIcon(soundon.mousedetection(mx,my));
			else
			soundoff.drawIcon(soundoff.mousedetection(mx, my)); 
			if (instruction.mousedetection(mx, my))
				al_draw_bitmap(instructionPanel,100,125,NULL);
			al_draw_text(font, MenuGray, ScreenWidth - 280, 580, NULL, "produced by macieopi");
			al_flip_display();
			al_clear_to_color(DarkBlue);

			
		}
	}
	
	
	done = false;
	C[0].xc = 250, C[0].yc = 260;
	al_hide_mouse_cursor(display);
	while (!done)
	{
	
		al_set_sample_instance_gain(songInstance, volume);
		al_wait_for_event(eventqueue, &events);
		al_get_keyboard_state(&keyState);

		if (events.type == ALLEGRO_EVENT_KEY_UP)
		{
			switch (events.keyboard.keycode)
			{
			case ALLEGRO_KEY_ESCAPE:
				
				
				if (pause)
				{
					al_stop_timer(timer);
					al_stop_timer(secTimer);
					pause = false;
				}
				else
				{
					al_resume_timer(timer);
					al_resume_timer(secTimer);
					pause = true;
				}
				break;
			}
		}

		else if (events.type == ALLEGRO_EVENT_DISPLAY_CLOSE)
			done = true;

		
		else if (events.type == ALLEGRO_EVENT_TIMER)
		{
			if (events.timer.source == timer)
			{
				active = true;

				if (al_key_down(&keyState, ALLEGRO_KEY_RIGHT))
				{
					p1.velx= movespeed;
					dir = RIGHT;

				}
				else if (al_key_down(&keyState, ALLEGRO_KEY_LEFT))
				{
					p1.velx = -movespeed;
					dir = LEFT;

				}
				else if (al_key_down(&keyState, ALLEGRO_KEY_DOWN))
				{
					p1.vely += (jumpspeed*0.4);
					dir = DOWN;
				}
				else
				{
					active = false;
					p1.velx = 0;
				}

				if (al_key_down(&keyState, ALLEGRO_KEY_UP) && jump)
				{
					p1.vely = -jumpspeed;
					jump = false;
				}
				if (al_key_down(&keyState, ALLEGRO_KEY_M))
				{
					if (sound)
					{
						sound = false;
						volume = 0;
					}
					else
					{
						sound = true;
						volume = 0.5;
					}
					al_rest(0.08);
				}

				if (!jump)
					p1.vely += gravity;
				else
					p1.vely = 0;
				p1.x += p1.velx;
				p1.y += p1.vely;

				jump = (p1.y + 48 >= groundHeight);
				if (jump)
					p1.y = groundHeight- 48;
				if (p1.x <= 0)
					p1.x = 0;
				if (p1.x >= ScreenWidth - 28)
					p1.x = ScreenWidth - 28;

				collision(p1, C[0], sound, counter);
				if (p1.score % 10 ==1 && counter%2 ==1 && p1.score > 10)
				{
					collision(p1, timerCoin, sound, counter);
					if (collision(p1, slowdownCoin, sound, counter))
					{
						slowdown = true;
					}
					slowdownCoin.CoinMovement(1,false);
					timerCoin.CoinMovement(2, false);
				}
				if (p1.score % 10 == 2 &&  p1.score > 10)
					counter = 1;
				C[0].CoinMovement(p1.level,false);
				if (chmurkiX > -2000)
					chmurkiX -= 0.4;
				else
					chmurkiX = ScreenWidth + 100;
					
				
			}
			else if (events.timer.source == secTimer)
			{
				degrees=-p1.sec*8;
				p1.sec--;				
				if (p1.sec <= 0)
					done = true;
				
				if (slowdown)
				{
					movespeed = 3;
					jumpspeed = 15;
					slowDownTimer++;
				}
				if (slowDownTimer >= 5)
				{
					jumpspeed = 20;
					movespeed = 5;
					slowDownTimer = 0;
					slowdown = false;
				}
			}

			else if (events.timer.source == FrameTimer)
			{
				
					if (active)
						p1.sourceX += al_get_bitmap_width(player) / 4;
					else
						p1.sourceX = 0;

					if (p1.sourceX >= al_get_bitmap_width(player))
						p1.sourceX = 0;

					p1.sourceY = dir;
			}
		
			if (p1.score == 5)
			{
				p1.level = 1;
			}
			else if (p1.score == 10)
			{
				p1.level = 2;
			}
			else if (p1.score == 20)
			{
				p1.level = 3;
			}
			

			draw = true;
		}


		if (draw)
		{
			
			draw = false;
			
			al_draw_bitmap(ground, 0, 0, NULL);
			al_draw_bitmap(chmurki, chmurkiX, 50, NULL);
			al_draw_bitmap(zegar, ScreenWidth - 120, 20, NULL);
			al_draw_rotated_bitmap(wskazowka, 50, 50, ScreenWidth - 70, 70, degrees*ALLEGRO_PI / 180, NULL);
			al_draw_bitmap_region(player, p1.sourceX, p1.sourceY*al_get_bitmap_height(player) / 4, 32, 48, p1.x, p1.y, NULL);
			//al_draw_bitmap(C[0].coin, C[0].xc, C[0].yc, NULL);
			C[0].DrawCoin();
						
			if (p1.score % 10 == 1 && counter % 2 == 1 && p1.score > 10)
			{
				timerCoin.DrawCoin();
				slowdownCoin.DrawCoin();
			}
			al_draw_textf(font2, MenuGray, 10, 20, NULL, "SCORE: %i LVL: %i", p1.score,p1.level);
			al_draw_textf(font, MenuGray,ScreenWidth-125 , 120, NULL, "TIME: %i",p1.sec);
			if (!pause)
			{
				al_draw_filled_rectangle(0, 0, 800, 600, al_map_rgba(0, 0, 0, 150));
				al_draw_text(font2, MenuGray, ScreenWidth/2-50, 250, NULL, "PAUSE");
			}
			al_draw_text(font, MenuGray, ScreenWidth - 280, 580,  NULL, "produced by macieopi");
			al_flip_display();
			al_clear_to_color(DarkBlue);
		}
	}
	al_draw_text(font2, MenuGray, ScreenWidth /3+20, 300, NULL, "GAME OVER");
	al_draw_text(font, MenuGray, ScreenWidth - 280, 580, NULL, "produced by macieopi");
	al_flip_display();
	al_clear_to_color(DarkBlue);
	al_rest(2.5);
	saveGame(p1);
	WinnerSave(winner, p1);
	

	al_rest(0.85);
	// ALLEGRO stuff
	al_destroy_font(font);
	al_destroy_font(font2);
	al_destroy_bitmap(player);
	al_destroy_bitmap(gierka);
	al_destroy_bitmap(chmurki);
	al_destroy_bitmap(zegar);
	al_destroy_bitmap(wskazowka);
	for (int i = 0; i < 4; i++) {
		al_destroy_sample(C[i].soundEffect);
		al_destroy_bitmap(C[i].coin);
	}
	al_destroy_bitmap(soundoff.FilledMenu);
	al_destroy_bitmap(soundon.FilledMenu);
	al_destroy_bitmap(soundoff.menu);
	al_destroy_bitmap(soundon.menu);
	al_destroy_bitmap(ground);
	al_destroy_timer(timer);
	al_destroy_timer(FrameTimer);
	al_destroy_timer(secTimer);
	al_destroy_display(display);
	
	
	al_destroy_sample(song);
	al_destroy_sample_instance(songInstance);

		
	
	return 0;
}