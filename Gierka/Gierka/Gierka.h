#pragma once
#include <iostream>
#include <allegro5/allegro.h>
#include <allegro5/allegro_primitives.h>
#include <allegro5/allegro_native_dialog.h>
#include <allegro5\allegro_image.h>
#include <allegro5\allegro_audio.h>
#include <allegro5\allegro_acodec.h>
#include <allegro5\allegro_font.h>
#include <allegro5\allegro_ttf.h>
#include <sstream>
#include <vector>
#include <string>
#include <algorithm>
#include <cstdlib>
#include <fstream>

using namespace std;
#define ScreenWidth 800
#define ScreenHeight 600

struct Menu	// Object for menu
{
	float x = 200, y = 300, width = 200, height = 50;
	ALLEGRO_BITMAP* menu;
	ALLEGRO_BITMAP* FilledMenu;
	ALLEGRO_FONT *font = al_load_font("assets\\fonts\\Roboto.ttf", 25, ALLEGRO_ALIGN_CENTER);
	void drawMenu(const char* text, ALLEGRO_COLOR textColor, ALLEGRO_COLOR AltwenativeTextColor, ALLEGRO_COLOR menuColor, bool filled)
	{
		if (!filled)
		{
			al_draw_rectangle(x, y, x + width, y + height, menuColor, 5); //al_map_rgb(192, 205, 226)
			al_draw_text(font, textColor, x + (width / 4), y + 7, NULL, text);
		}
		else
		{
			al_draw_filled_rectangle(x, y, x + width, y + height, menuColor); //al_map_rgb(192, 205, 226)
			al_draw_text(font, AltwenativeTextColor, x + (width / 4), y + 7, NULL, text);
		}
	}
	void drawIcon(bool filled)
	{
		if (!filled)
			al_draw_bitmap(menu, x, y, NULL);
		else
			al_draw_bitmap(FilledMenu, x, y, NULL);
	}

	bool mousedetection(float mx, float my)
	{
		if (mx < x || mx > x + width || my < y || my > y + height)
			return false;
		else
			return true;
	}
};
struct Winner	// simple data type for saving
{
	int score=0;
	int level=0;
	//char name[35];

	inline bool operator > (const Winner& winner) const
	{
	return (score > winner.score);
	}
};
struct Player	// Player object
{
	float x = 400, y = 10;
	int score = 0;
	int level = 0;
	int sec=45;
	float sourceX = 32, sourceY = 0;
	float velx = 0, vely = 0;
	char name[35];
};
struct Coin //Coin type objects
{
	bool horizontalMovement = true, verticalMovement = true;
	int xc = 500, yc = 260, velocity = 5;
	ALLEGRO_BITMAP *coin = al_load_bitmap("assets\\bitmaps\\coin.png");
	bool timeCoin = false,slowdownCoin=false;
	ALLEGRO_SAMPLE * soundEffect = al_load_sample("assets\\sounds\\mario.wav");
	int width = al_get_bitmap_width(coin), height = al_get_bitmap_height(coin);

	void DrawCoin()
	{
		al_draw_bitmap(coin, xc, yc, NULL);
	}
	void CoinMovement(int lvl, bool menu)
	{
		if (lvl >= 1)
		{
			if (xc >= ScreenWidth - width)

				horizontalMovement = false;
			else if (xc <= 0 + width)
				horizontalMovement = true;

			if (horizontalMovement)
				xc += velocity;
			else
				xc -= velocity;
		}
		if (lvl >= 2)
		{
			if (menu)
			{
				if (yc >= ScreenHeight - height)
				{
					verticalMovement = false;
				}
			}
			else
				if (yc >= 420 - height)
				{
					verticalMovement = false;
				}
			if (yc <= 0)
				verticalMovement = true;

			if (verticalMovement)
				yc += velocity;
			else
				yc -= velocity;
		}
	}
};

void saveGame(Player player); // saving game state to binary file
void loadGame(Player & player); // loading game state from binary file
void WinnerLoad(vector<Winner> & winner);	// loading score from binary file
void WinnerSave(vector<Winner> & winner, Player player);// saving score to binary file
void WinnerShow(vector <Winner> winner);		//console representation of new highscore
bool collision(Player &player, Coin& coin, bool sound, int &counter);// Checking the collision 

